//@ts-ignore
const browser = browser;

const label: string = browser.i18n.getMessage("context");
const urlWhatsAppWeb = "https://web.whatsapp.com/";
const supportsSpacesToolbar = !!browser.spacesToolbar;
const spacesToolbarButtonId = "wa_in_th_" + (Math.floor(Math.random() * 99999999)).toString();
// Modified User Agent
// This is necessary as WhatsApp Web only accepts few web browsers.
const ua = navigator.userAgent.includes("Firefox") ?
    navigator.userAgent.slice(0, navigator.userAgent.lastIndexOf("Thunderbird")) :
    navigator.userAgent.replace("Thunderbird", "Firefox");

let hasSpacesToolbarButton: boolean = false;
let iconPath: string = "icons/icon-green.svg";

function initialize(): void {
    setWaInThMode();
    setIcon();
    createContextMenu();
    setSpacesToolbarButton();
}

function setWaInThMode(): void {
    let getWaInThMode = browser.storage.local.get("wa-in-th-mode");
    getWaInThMode.then((storedValue: any) => {
        if (storedValue["wa-in-th-mode"] === "popup") {
            browser.browserAction.setPopup({ popup: urlWhatsAppWeb });
        } else {
            browser.browserAction.setPopup({ popup: "" });
            browser.storage.local.set({ "wa-in-th-mode": "tab" });
        }
    });
}

function setIcon(): void {
    if (browser.browserAction.setIcon) {
        let getWaInThIcon: any = browser.storage.local.get("wa-in-th-icon")

        getWaInThIcon.then((storedValue: any) => {
            const preferredIcon: string = storedValue["wa-in-th-icon"];
            if (preferredIcon) {
                iconPath = `icons/icon-${storedValue["wa-in-th-icon"]}.svg`;
                browser.browserAction.setIcon({ path: iconPath });
                updateSpacesToolbarButton();
            } else {
                browser.storage.local.set({ "wa-in-th-icon": "green" });
            }
        });
    }
}

function createContextMenu(): void {
    browser.menus.create({
        id: "contextMenuEntry",
        title: browser.i18n.getMessage("context"),
        type: "normal",
        contexts: ["browser_action"],
        onclick: createOrActivateTab
    }, console.log("WhatsApp Web context menu created."));
}

function setSpacesToolbarButton(): void {
    let getWaInThMode = browser.storage.local.get("wa-in-th-spaces-toolbar");
    getWaInThMode.then((storedValue: any) => {
        const isSpacesToolbar = storedValue["wa-in-th-spaces-toolbar"];
        if (isSpacesToolbar === "true") {
            if (hasSpacesToolbarButton) {
                updateSpacesToolbarButton();
            } else {
                addSpacesToolbarButton();
            }
            return;
        }
        if (isSpacesToolbar === "false") {
            if (hasSpacesToolbarButton) {
                removeSpacesToolbarButton();
            }
            return;
        }

        browser.storage.local.set({ "wa-in-th-spaces-toolbar": "true" });
        addSpacesToolbarButton();
    });
}

function getButtonProperties(): Object {
    return {
        defaultIcons: {
            16: iconPath,
            32: iconPath
        },
        title: label,
        url: urlWhatsAppWeb
    };
}

async function closeDuplicates(newTabId: number) {
    const queryTabs: any[] = await browser.tabs.query({ url: urlWhatsAppWeb });
    const duplicateTabs: number[] = queryTabs.map((tab: any) => tab.id);
    if (duplicateTabs.includes(newTabId)) {
        browser.tabs.remove(duplicateTabs.filter((tabId: number) => tabId !== newTabId));
    }
}

async function addSpacesToolbarButton(): Promise<void> {
    if (supportsSpacesToolbar) {
        browser.spacesToolbar.addButton(
            spacesToolbarButtonId,
            getButtonProperties()
        );
        console.log("WhatsApp Web spaces toolbar menu created.");
    } else {
        console.log("spacesToolbar is not supported...\n");
    }
    hasSpacesToolbarButton = true;
}

function removeSpacesToolbarButton(): void {
    if (supportsSpacesToolbar) {
        browser.spacesToolbar.removeButton(spacesToolbarButtonId);
        console.log("WhatsApp Web spaces toolbar menu removed.");
    } else {
        console.log("spacesToolbar is not supported...\n");
    }
    hasSpacesToolbarButton = false;
}

function updateSpacesToolbarButton(): void {
    if (supportsSpacesToolbar) {
        browser.spacesToolbar.updateButton(
            spacesToolbarButtonId,
            getButtonProperties()
        );
        console.log("WhatsApp Web spaces toolbar menu refreshed.");
    } else {
        console.log("spacesToolbar is not supported...\n");
    }
}

async function createOrActivateTab(): Promise<void> {

    let tabId: number = NaN;

    const queryTabs: any[] = await browser.tabs.query({ url: urlWhatsAppWeb });
    if (queryTabs.length > 0) {
        for (const tab of queryTabs) {
            tabId = tab.id;
            break;
        }
    }

    if (isNaN(tabId)) {
        if (supportsSpacesToolbar && hasSpacesToolbarButton && !!browser.spacesToolbar.clickButton) {
            browser.spacesToolbar.clickButton(spacesToolbarButtonId);
        } else {
            browser.tabs.create({
                active: true,
                url: urlWhatsAppWeb,
            });
        }
    } else {
        browser.tabs.update(tabId, {
            active: true
        });
    }
}

function onStorageChange(item: any) {
    if (item.hasOwnProperty("wa-in-th-mode")) {
        item["wa-in-th-mode"].newValue === "popup" ?
            browser.browserAction.setPopup({ popup: urlWhatsAppWeb }) :
            browser.browserAction.setPopup({ popup: "" });
    }
    if (item.hasOwnProperty("wa-in-th-spaces-toolbar")) {
        setSpacesToolbarButton();
    }
    if (item.hasOwnProperty("wa-in-th-icon")) {
        setIcon();
    }
}

/**
 * Modify user agent
 * Source : https://github.com/mdn/webextensions-examples/tree/master/user-agent-rewriter
 */
function rewriteUserAgentHeader(e: any) {
    for (let header of e.requestHeaders) {
        if (header.name.toLowerCase() === "user-agent") {
            header.value = ua;
        }
    }
    return { requestHeaders: e.requestHeaders };
}


/* Initialization sequence */
initialize();

browser.storage.onChanged.addListener(onStorageChange);
browser.browserAction.onClicked.addListener(createOrActivateTab);
browser.tabs.onUpdated.addListener(closeDuplicates);
browser.webRequest.onBeforeSendHeaders.addListener(
    rewriteUserAgentHeader,
    { urls: [urlWhatsAppWeb] },
    ["blocking", "requestHeaders"]
);
