[Thank you for reporting an issue! This templates aims at helping reporting information-rich issues. Please delete the instructions in square brackets and replace them with your content 🙂]

# Description and reproduction of the issue

[Describe here the abnormal behavior that you observe and how to trigger it. It is also useful to precise if the issue arrived at a given point in time or following to some event, and if you could reproduce it on another machine.]

# Expected behavior

[Describe here the normal behavior that you would expect]

# Supporting screenshot(s)

[Add additional screenshots that will help understanding what you observe and in which context]

# Supporting information

[Add additional information about the system on which you observe the issue. Most is available in Help menu > Troubleshooting information.]
- Cookies settings (Settings > Privacy & Security > Web content section + exceptions related to WhatsApp Web if you don't accept all cookies)

- Exact OS name and version (e.g. Windows 11 22H2)

- Thunderbird version (e.g. 115.3.2 64 bits)

- WhatsApp Web in Thunderbird version (e.g. 1.3.2)

- User agent (e.g. Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Thunderbird/102.8.0)
